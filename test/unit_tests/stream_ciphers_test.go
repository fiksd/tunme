package unit_tests

import (
	"bytes"
	"container/list"
	"io"
	"math/rand"
	"sync"
	"testing"
	"tunme/pkg/tunme"
	"tunme/pkg/tunme_app"
)

func closeOrFail(t *testing.T, closer io.Closer) {
	if closer.Close() != nil {
		t.FailNow()
	}
}

type streamTestProgram struct {
	_total int
	_list  list.List
}

func (p *streamTestProgram) Write(n int) {
	p._total += n
	p._list.PushBack(n)
}

func (p *streamTestProgram) Flush() {
	p._list.PushBack(nil)
}

func (p *streamTestProgram) _doDecrypt(t *testing.T, cipherFactory tunme.CipherFactory, reader io.ReadCloser, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	defer closeOrFail(t, reader)

	decryptor := cipherFactory.CreateStreamDecryptor(reader, 1000)

	rnd := rand.New(rand.NewSource(4681))

	total := 0

	var buff [256]byte

	for {
		n, err := decryptor.Read(buff[:])

		total += n

		ref := make([]byte, n)
		rnd.Read(ref)

		if bytes.Compare(buff[:n], ref) != 0 {
			t.Log("the received data does not match what was sent")
			t.FailNow()
		}

		if err == io.EOF {
			break
		} else if err != nil {
			t.Log("got an error while reading")
			t.FailNow()
		}
	}

	if total != p._total {
		t.Log("expecting to receive ", p._total, " byte(s), but got ", total)
		t.FailNow()
	}
}

func (p *streamTestProgram) _doEncrypt(t *testing.T, cipherFactory tunme.CipherFactory, writer io.WriteCloser, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	defer closeOrFail(t, writer)

	encryptor := cipherFactory.CreateStreamEncryptor(writer, 1000)
	defer closeOrFail(t, encryptor)

	rnd := rand.New(rand.NewSource(4681))

	for e := p._list.Front(); e != nil; e = e.Next() {

		if e.Value == nil {
			err := encryptor.Flush()
			if err != nil {
				t.Log("got an error while flushing: ", err)
				t.FailNow()
			}
		} else {
			buff := make([]byte, e.Value.(int))
			rnd.Read(buff)
			_, err := encryptor.Write(buff)
			if err != nil {
				t.Log("got an error while writing: ", err)
				t.FailNow()
			}
		}
	}

}

func (p *streamTestProgram) Run(t *testing.T) {

	key := make([]byte, 32)
	cipherFactory := tunme_app.CreateFixedKeyCipherFactory(key)

	rd, wr := io.Pipe()

	var waitGroup sync.WaitGroup
	defer waitGroup.Wait()

	waitGroup.Add(1)
	go p._doDecrypt(t, cipherFactory, rd, &waitGroup)

	waitGroup.Add(1)
	go p._doEncrypt(t, cipherFactory, wr, &waitGroup)
}

// TODO: vary size of reads

func TestCreatingStreams(t *testing.T) {

	var program streamTestProgram

	program.Run(t)
}

func TestSending(t *testing.T) {

	var program streamTestProgram

	program.Write(4)

	program.Run(t)
}

func TestFlush(t *testing.T) {

	var program streamTestProgram

	program.Write(4)
	program.Flush()
	program.Write(4)

	program.Run(t)
}

func TestFlushOnly(t *testing.T) {

	var program streamTestProgram

	program.Flush()

	program.Run(t)
}

func TestBig(t *testing.T) {

	var program streamTestProgram

	program.Write(20000)

	program.Run(t)
}

func TestMisc(t *testing.T) {

	var program streamTestProgram

	program.Flush()
	program.Write(20000)
	program.Write(20000)
	program.Write(2)
	program.Write(0)
	program.Write(374)
	program.Flush()
	program.Write(100000)

	program.Run(t)
}
