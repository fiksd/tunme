package tunme_app

import (
	"bufio"
	"crypto/rand"
	"fmt"
	"io"
	"math/big"
	"os"
	"salsa.debian.org/vasudev/gospake2"
	"tunme/pkg/tunme"
)

type _passwordCipherFactory struct {
	_identity            gospake2.IdentityS
	_spake2              gospake2.SPAKE2
	_isSpake2Initialized bool
	_factory             tunme.CipherFactory
}

func CreatePasswordCipherFactory() tunme.CipherFactory {

	return &_passwordCipherFactory{
		_identity: gospake2.NewIdentityS("b4f0a7b0-d630-4272-aefa-5cbf9fc581de"),
	}
}

func (fac *_passwordCipherFactory) IsHandshakeFinished() bool {
	return fac._factory != nil
}

func (fac *_passwordCipherFactory) NextHandshakeStep(received []byte) ([]byte, error) {

	if fac.IsHandshakeFinished() {
		return nil, fmt.Errorf("the handshake is already finished")
	}

	if received == nil {

		// first step on the client

		a, err := rand.Int(rand.Reader, big.NewInt(999))
		if err != nil {
			return nil, err
		}
		b, err := rand.Int(rand.Reader, big.NewInt(999))
		if err != nil {
			return nil, err
		}
		password := fmt.Sprintf("%d.%d", a, b)

		fmt.Printf("Password: %s\n", password)

		fac._spake2 = gospake2.SPAKE2Symmetric(gospake2.NewPassword(password), fac._identity)
		fac._isSpake2Initialized = true

		return fac._spake2.Start(), nil

	} else if !fac._isSpake2Initialized {

		// single step on the server

		// TODO: use TTY

		fmt.Printf("Password: ")

		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		if err := scanner.Err(); err != nil {
			return nil, err
		}

		fac._spake2 = gospake2.SPAKE2Symmetric(gospake2.NewPassword(scanner.Text()), fac._identity)
		fac._isSpake2Initialized = true

		response := fac._spake2.Start()

		key, err := fac._spake2.Finish(received)
		if err != nil {
			return nil, err
		}

		fac._factory = CreateFixedKeyCipherFactory(key)

		return response, nil

	} else {

		// second step on the client

		key, err := fac._spake2.Finish(received)
		if err != nil {
			return nil, err
		}

		fac._factory = CreateFixedKeyCipherFactory(key)

		return nil, nil
	}
}

func (fac *_passwordCipherFactory) CreateStreamEncryptor(output io.Writer, bufferSize int) tunme.StreamEncryptor {
	return fac._factory.CreateStreamEncryptor(output, bufferSize)
}

func (fac *_passwordCipherFactory) CreateStreamDecryptor(input io.Reader, bufferSize int) tunme.StreamDecryptor {
	return fac._factory.CreateStreamDecryptor(input, bufferSize)
}
