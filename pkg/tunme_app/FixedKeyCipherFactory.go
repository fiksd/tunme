package tunme_app

import (
	"fmt"
	"io"
	"tunme/internal"
	"tunme/pkg/tunme"
)

type _fixedKeyCipherFactory struct {
	key    [32]byte
}

func CreateFixedKeyCipherFactory(key []byte) tunme.CipherFactory {
	var factory _fixedKeyCipherFactory
	copy(factory.key[:], key)
	return &factory
}

func (factory *_fixedKeyCipherFactory) IsHandshakeFinished() bool {
	return true
}

func (factory *_fixedKeyCipherFactory) NextHandshakeStep([]byte) ([]byte, error) {
	return nil, fmt.Errorf("handshake already finished")
}

func (factory *_fixedKeyCipherFactory) CreateStreamEncryptor(output io.Writer, bufferSize int) tunme.StreamEncryptor {
	return internal.CreateStreamEncryptorImpl(output, bufferSize, factory.key[:])
}

func (factory *_fixedKeyCipherFactory) CreateStreamDecryptor(input io.Reader, bufferSize int) tunme.StreamDecryptor {
	return internal.CreateStreamDecryptorImpl(input, bufferSize, factory.key[:])
}
