package tunme

import "io"

type ClosedErrorType struct {
}

var ClosedError ClosedErrorType

func (err ClosedErrorType) Error() string {
	return "the object has been closed"
}

type Link interface {
	io.Closer
	GetLinkInfo() *LinkInfo

	ReceiveDatagram() ([]byte, error)
	SendDatagram(payload []byte) error

	OpenStream() (Stream, error)
	AcceptStream() (Stream, error)
}
