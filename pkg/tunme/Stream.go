package tunme

import "io"

// Implementations should be completely thread-safe, under the assumption that:
// * Send() is not called twice simultaneously.
// * Read() is not called twice simultaneously.

type Stream interface {
	GetId() uint64
	SetAttachedData(data interface{})
	GetAttachedData() interface{}

	io.ReadWriteCloser
	Flush() error
}
