package tunme

type Module interface {
	CreateLink(args []string, services Services) (Link, error)
}
