package main

import (
	"fmt"
	"io"
	"os"
	"sync"
	"tunme/pkg/tunme"
	"tunme/pkg/tunme_app"
)

func linkToInterface(link tunme.Link, iface io.Writer) {

	for {
		datagram, err := link.ReceiveDatagram()
		if err != nil {
			panic(err)
		}

		_, err = iface.Write(datagram)
		if err != nil {
			// TODO
		}
	}
}

func interfaceToLink(link tunme.Link, iface io.Reader) {

	buff := make([]byte, 10000) // TODO

	for {
		n, err := iface.Read(buff)

		if n != 0 {
			link.SendDatagram(buff[:n])
		}

		if err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}
	}
}

func run(link tunme.Link, iface io.ReadWriter) {

	var waitGroup sync.WaitGroup
	defer waitGroup.Wait()

	waitGroup.Add(1)
	go func() {
		linkToInterface(link, iface)
		waitGroup.Done()
	}()

	interfaceToLink(link, iface)
}

func createInterface(args []string) io.ReadWriteCloser {

	// TODO: improve & document arg parsing

	// [OPTION...] [ADDRESS]

	optFlagPing := false

	var optAddress *string

	for _, arg := range args {
		if arg == "--ping" {
			optFlagPing = true
		} else if arg[:1] != "-" {
			if optAddress != nil {
				panic("duplicate argument")
			}
			optAddress = &arg
		} else {
			panic(fmt.Errorf("unrecognized argument: %s", arg))
		}
	}

	if optFlagPing {

		return CreatePingResponder()

	} else {

		tun, err := CreateTunDevice()
		if err != nil {
			panic(err)
		}

		fmt.Printf("Created device %s.\n", tun.Name())

		if optAddress != nil {

			err = ConfigureInterface(tun.Name(), *optAddress)
			if err != nil {
				// TODO: warn only?
				panic(err)
			}

			fmt.Printf("Configured device with address %s.\n", *optAddress)
		}

		return tun
	}
}

func createLink(args []string) tunme.Link {

	var app tunme_app.App

	link, err := app.CreateLink(args[0], args[1:])
	if err != nil {
		panic(err)
	}

	return link
}

func main() {

	appArgCount := 0

	for 1+appArgCount < len(os.Args)-1 && os.Args[1+appArgCount] != "--" {
		appArgCount++
	}
	if 1+appArgCount >= len(os.Args) {
		panic(0) // TODO
	}

	iface := createInterface(os.Args[1 : 1+appArgCount])
	defer iface.Close()

	link := createLink(os.Args[1+appArgCount+1:])
	defer link.Close()

	run(link, iface)
}
