package lib

import (
	"sync"
	"tunme/pkg/tunme"
)

type TestLink struct {
	link                tunme.Link
	testStreamManager   *TestStreamManager
	receivedStreamsChan chan tunme.Stream
	waitGroup           sync.WaitGroup
}

func CreateTestLink(link tunme.Link) *TestLink {

	lnk := &TestLink{
		link:                link,
		testStreamManager:   createTestStreamManager(),
		receivedStreamsChan: make(chan tunme.Stream),
	}

	lnk.waitGroup.Add(1)
	go lnk._loop()

	return lnk
}

func (lnk *TestLink) Close() error {

	err := lnk.link.Close() // TODO: how to handle streams which are still opened?
	if err != nil {
		return err
	}

	lnk.waitGroup.Wait()

	return nil
}

func (lnk *TestLink) OpenStream() *TestStream {

	stream1, err := lnk.link.OpenStream()
	if err != nil {
		panic(err)
	}

	stream2 := <-lnk.receivedStreamsChan

	return lnk.testStreamManager.CreateTestStream(stream1, stream2)
}

func (lnk *TestLink) _loop() {

	defer lnk.waitGroup.Done()

	for {

		stream, err := lnk.link.AcceptStream()
		if err != nil {
			break
		}

		lnk.receivedStreamsChan <- stream
	}
}
