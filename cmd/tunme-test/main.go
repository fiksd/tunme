package main

import (
	"os"
	"tunme/pkg/tunme_app"
)

func main() {

	var app tunme_app.App

	link, err := app.CreateLink(os.Args[1], os.Args[2:])
	if err != nil {
		panic(err)
	}
	defer link.Close()

	if link.GetLinkInfo().IsServer {
		runServer(link)
	} else {
		runClient(link)
	}
}
