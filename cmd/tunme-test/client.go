package main

import (
	"io"
	"sync"
	"tunme/cmd/tunme-test/lib"
	"tunme/pkg/tunme"
)

// TODO: move
func CloseOrPanic(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		panic(err)
	}
}

func testStreamOpenClose(link *lib.TestLink, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	stream := link.OpenStream()
	defer stream.Close()
}

func testStreamSmallWrite(link *lib.TestLink, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	stream := link.OpenStream()
	defer stream.Close()

	stream.Send(10)
}

func testStreamLargeWrite(link *lib.TestLink, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	stream := link.OpenStream()
	defer stream.Close()

	stream.Send(20000)
}

func testStreamReadAndWrite(link *lib.TestLink, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	stream := link.OpenStream()
	defer stream.Close()

	stream.Send(200)
	stream.Receive(20)
	stream.Receive(564)
	stream.Send(569)
	stream.Receive(1)
}

func testStreamSmallRead(link *lib.TestLink, waitGroup *sync.WaitGroup) {

	defer waitGroup.Done()

	stream := link.OpenStream()
	defer stream.Close()

	stream.Receive(10)
}

func runClient(link tunme.Link) {

	lnk := lib.CreateTestLink(link)
	defer CloseOrPanic(lnk)

	var waitGroup sync.WaitGroup
	defer waitGroup.Wait()

	waitGroup.Add(5)
	go testStreamOpenClose(lnk, &waitGroup)
	go testStreamSmallWrite(lnk, &waitGroup)
	go testStreamSmallRead(lnk, &waitGroup)
	go testStreamLargeWrite(lnk, &waitGroup)
	go testStreamReadAndWrite(lnk, &waitGroup)
}
