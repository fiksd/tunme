package main

import (
	"fmt"
	"os"
	"sync"
	"tunme/pkg/tunme"
	"tunme/pkg/tunme_app"
)

func relayDatagramsOneWay(link1 tunme.Link, link2 tunme.Link) error {

	for {
		datagram, err := link1.ReceiveDatagram()
		if err != nil {
			return err
		}

		err = link2.SendDatagram(datagram)
		if err != nil {
			return err
		}
	}
}

func relayStreamOneWay(stream1 tunme.Stream, stream2 tunme.Stream) error {

	buff := make([]byte, 4096)

	for {

		n, readErr := stream1.Read(buff)

		if n != 0 {

			_, err := stream2.Write(buff[:n])
			if err != nil {
				return err
			}

			err = stream2.Flush()
			if err != nil {
				return err
			}
		}

		if readErr != nil {
			return readErr
		}
	}
}

func relayStreamsOneWay(link1 tunme.Link, link2 tunme.Link) error {

	var waitGroup sync.WaitGroup
	defer waitGroup.Wait()

	for {
		stream1, err := link1.AcceptStream()
		if err != nil {
			return err
		}

		stream2, err := link2.OpenStream()
		if err != nil {
			return err
		}

		waitGroup.Add(1)
		go func() {
			defer waitGroup.Done()
			defer stream1.Close()
			err := relayStreamOneWay(stream1, stream2)
			if err != tunme.ClosedError {
				panic(err)
			}
		}()

		waitGroup.Add(1)
		go func() {
			defer waitGroup.Done()
			defer stream2.Close()
			err := relayStreamOneWay(stream2, stream1)
			if err != tunme.ClosedError {
				panic(err)
			}
		}()
	}
}

func relay(link1 tunme.Link, link2 tunme.Link) {

	var waitGroup sync.WaitGroup
	defer waitGroup.Wait()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		err := relayStreamsOneWay(link1, link2)
		if err != tunme.ClosedError {
			panic(err)
		}
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		err := relayStreamsOneWay(link2, link1)
		if err != tunme.ClosedError {
			panic(err)
		}
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		err := relayDatagramsOneWay(link1, link2)
		if err != tunme.ClosedError {
			panic(err)
		}
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		err := relayDatagramsOneWay(link2, link1)
		if err != tunme.ClosedError {
			panic(err)
		}
	}()
}

func createAndRelay(args1 []string, args2 []string) {

	fmt.Println("- peer 1 -")

	var app1 tunme_app.App
	link1, err := app1.CreateLink(args1[0], args1[1:])
	if err != nil {
		panic(err)
	}
	defer link1.Close()

	fmt.Println("- peer 2 -")

	var app2 tunme_app.App
	link2, err := app2.CreateLink(args2[0], args2[1:])
	if err != nil {
		panic(err)
	}
	defer link2.Close()

	fmt.Println("- relay -")

	relay(link1, link2)
}

func main() {

	sep := 1

	for sep < len(os.Args) && os.Args[sep] != "--" {
		sep++
	}

	// TODO: handle bad usage

	createAndRelay(os.Args[1:sep], os.Args[sep+1:])
}
