
### Encrypted Stream Chunk

| Field              | Size         | Description                                                                  |
|--------------------|--------------|------------------------------------------------------------------------------|
| [nonce]            | 16 bytes     | Only present on the first chunk.                                             |
| size               | 2 bytes      | Size of the ciphertext.                                                      |
| is_last_chunk      | 1 byte       | Set to 1 on the last chunk, 0 otherwise.                                     |
| ciphertext         | *size* bytes | Ciphertext, with the same size as the plaintext, and without authentication. |
| authentication_tag | 32 bytes     | A keyed hash of the data.                                                    |
