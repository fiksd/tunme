package internal

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/nacl/auth"
	"hash"
	"io"
	"tunme/internal/utils"
)

// TODO: once an error has been returned, the object should be unusable
// TODO: we might as well authenticate the chunk size

// Every write to the underlying stream has the following structure :
// * [nonce]:             only present in the first chunk
// * size:                2 bytes, unsigned, big endian, represents the length of the plaintext (without header values)
// * is_last_chunk:       1 byte, 0 or 1
// * payload:             $size bytes
// * authentication_tag

// length of the header without the optional nonce
const _streamCipherFixedHeaderLen = 3

type StreamEncryptorImpl struct {
	output io.Writer

	// buffering
	buffer                []byte
	bufferCiphertextBegin int
	bufferCiphertextEnd   int
	bufferContainsNonce   bool
	isClosed              bool // TODO: forbid writing if closed

	// cryptography
	cipher cipher.Stream
	hash   hash.Hash
}

func CreateStreamEncryptorImpl(output io.Writer, bufferSize int, key []byte) *StreamEncryptorImpl {

	hashFunc, err := blake2b.New(32, key)
	if err != nil {
		panic(err)
	}

	blockCipher, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	nonce := make([]byte, blockCipher.BlockSize())
	_, err = rand.Read(nonce)
	if err != nil {
		panic(err)
	}

	// /!\ this should stay consistent with calculations on the decryption side
	bufferSize = utils.Max(bufferSize, len(nonce)+_streamCipherFixedHeaderLen+auth.Size) // strictly minimal size
	bufferSize = utils.Max(bufferSize, (_streamCipherFixedHeaderLen+auth.Size)*4)        // minimal size for reasonable overhead

	buffer := make([]byte, bufferSize)

	copy(buffer, nonce)

	return &StreamEncryptorImpl{
		output:                output,
		buffer:                buffer,
		bufferCiphertextBegin: len(nonce) + _streamCipherFixedHeaderLen,
		bufferCiphertextEnd:   len(nonce) + _streamCipherFixedHeaderLen,
		cipher:                cipher.NewOFB(blockCipher, nonce),
		hash:                  hashFunc,
	}
}

func (encryptor *StreamEncryptorImpl) Write(p []byte) (int, error) {

	n := 0

	for n < len(p) {

		chunkLen := utils.Min(len(p)-n, len(encryptor.buffer)-encryptor.hash.Size()-encryptor.bufferCiphertextEnd)

		if chunkLen == 0 {
			err := encryptor._flush()
			if err != nil {
				return 0, err
			}
		} else {
			encryptor.cipher.XORKeyStream(encryptor.buffer[encryptor.bufferCiphertextEnd:], p[n:n+chunkLen])
			encryptor.hash.Write(encryptor.buffer[encryptor.bufferCiphertextEnd : encryptor.bufferCiphertextEnd+chunkLen])
			encryptor.bufferCiphertextEnd += chunkLen
			n += chunkLen
		}
	}

	return n, nil
}

func (encryptor *StreamEncryptorImpl) Close() error {

	if encryptor.isClosed {
		panic("cannot close multiple times")
	}

	encryptor.isClosed = true

	return encryptor._flush()
}

func (encryptor *StreamEncryptorImpl) Flush() error {
	return encryptor._flush()
}

func (encryptor *StreamEncryptorImpl) _flush() error {

	// TODO: handle flush without data (and without close)

	// writing the ciphertext size to the buffer

	ciphertextLen := encryptor.bufferCiphertextEnd - encryptor.bufferCiphertextBegin
	encryptor.buffer[encryptor.bufferCiphertextBegin-_streamCipherFixedHeaderLen] = byte(ciphertextLen >> 8)
	encryptor.buffer[encryptor.bufferCiphertextBegin-_streamCipherFixedHeaderLen+1] = byte(ciphertextLen)

	// writing the is_last_chunk flag to the buffer
	// TODO: authenticate it

	isLastChunkFlag := byte(0)
	if encryptor.isClosed {
		isLastChunkFlag = 1
	}
	encryptor.buffer[encryptor.bufferCiphertextBegin-_streamCipherFixedHeaderLen+2] = isLastChunkFlag

	// writing the authentication tag to the buffer

	bufferEnd := encryptor.bufferCiphertextEnd + encryptor.hash.Size()
	encryptor.hash.Sum(encryptor.buffer[:encryptor.bufferCiphertextEnd])

	// flushing the buffer

	_, err := encryptor.output.Write(encryptor.buffer[:bufferEnd])
	if err != nil {
		return err
	}

	encryptor.bufferCiphertextBegin = _streamCipherFixedHeaderLen
	encryptor.bufferCiphertextEnd = encryptor.bufferCiphertextBegin

	//

	// TODO: reset the hash ?

	return nil
}
