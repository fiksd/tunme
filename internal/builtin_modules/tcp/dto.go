package tcp

import (
	"fmt"
	"io"
	"tunme/internal/utils"
)

func readUInt64(reader io.Reader) (uint64, error) {

	var val uint64

	var buff [8]byte
	n, err := reader.Read(buff[:])
	if err != nil {
		return 0, err
	} else if n != len(buff) {
		return 0, fmt.Errorf("failed to read enough bytes")
	}

	for i := 0; i < len(buff); i++ {
		val <<= 8
		val |= uint64(buff[i])
	}

	return val, nil
}

func writeUInt64(writer io.Writer, value uint64) error {

	var buff [8]byte

	for i := len(buff) - 1; i >= 0; i-- {
		buff[i] = byte(value)
		value >>= 8
	}

	_, err := writer.Write(buff[:])
	if err != nil {
		return err
	}

	return nil
}

// ===== Stream Hello =====

type streamHelloDto struct {
	streamId uint64
}

const streamHelloDtoSize = 8

func (dto *streamHelloDto) read(reader io.Reader) error {

	streamId, err := readUInt64(reader)
	if err != nil {
		return err
	}

	dto.streamId = streamId

	return nil
}

func (dto *streamHelloDto) write(writer io.Writer) error {

	err := writeUInt64(writer, dto.streamId)
	if err != nil {
		return err
	}

	return nil
}

// ===== Control Messages =====

type controlMsgType byte

const (
	controlMsgTypeDatagram controlMsgType = iota
	controlMsgTypeOpenStream
	_controlMsgTypeCount
)

func readControlMsgType(reader io.Reader) (controlMsgType, error) {

	var buff [1]byte

	_, err := reader.Read(buff[:])
	if err != nil {
		return 0, err
	}

	if buff[0] >= byte(_controlMsgTypeCount) {
		return 0, fmt.Errorf("invalid control message type")
	}

	return controlMsgType(buff[0]), nil
}

func writeControlMsgType(writer io.Writer, value controlMsgType) (int, error) {
	var buff [1]byte
	buff[0] = byte(controlMsgTypeOpenStream)
	return writer.Write(buff[:])
}

type controlMsgDatagramDto struct {
	datagram []byte
}

func (dto *controlMsgDatagramDto) read(reader io.Reader) error {

	// size

	buff := make([]byte, utils.BuffLenBytes)
	ok, err := utils.ReadBytes(reader, buff)
	if !ok {
		return err
	}
	datagramSize, err := utils.ParseBuffLen(buff)
	if err != nil {
		return err
	}

	// TODO: check max size

	// data

	buff = make([]byte, datagramSize)
	ok, err = utils.ReadBytes(reader, buff)
	if !ok {
		return err
	}
	dto.datagram = buff

	//

	return nil
}

func (dto *controlMsgDatagramDto) write(writer io.Writer) error {

	// size

	// TODO: check max size

	buff := make([]byte, utils.BuffLenBytes)
	utils.SerializeBuffLen(len(dto.datagram), buff)
	_, err := writer.Write(buff)
	if err != nil {
		return err
	}

	// data

	_, err = writer.Write(dto.datagram)
	if err != nil {
		return err
	}

	//

	return nil
}

type controlMsgOpenStreamDto struct {
	streamId uint64
}
func (dto *controlMsgOpenStreamDto) read(reader io.Reader) error {

	streamId, err := readUInt64(reader)
	if err != nil {
		return err
	}
	dto.streamId = streamId

	return nil
}

func (dto *controlMsgOpenStreamDto) write(writer io.Writer) error {

	err := writeUInt64(writer, dto.streamId)
	if err != nil {
		return err
	}

	return nil
}
