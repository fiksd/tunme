package tcp

import (
	"fmt"
	"io"
	"math"
	"net"
	"sync"
	"tunme/pkg/tunme"
)

// operation IDs used for controlling the lifecycle
const (
	_linkImplOpControlReceiveLoop int = iota
	_linkImplOpAcceptStream
	_linkImplOpCount
)

type linkImpl interface {
	tunme.Link
}

type streamAcceptor interface {
	io.Closer

	// Accept can be called from multiple places concurrently.
	Accept() (tunme.Stream, error)
}

type _linkImpl struct {
	_serverAddress        *net.TCPAddr
	_services             tunme.Services
	_ctrl                 controlStream
	_streams              streamManager
	_streamAcceptor       streamAcceptor
	_clientStreamAcceptor clientStreamAcceptor
	_datagramsChan        chan []byte
	_lifecycle            closerLifecycle
	_closeOnce            sync.Once
}

func createLinkImpl(
	tcpListener *net.TCPListener,
	serverAddress *net.TCPAddr,
	services tunme.Services,
	ctrl controlStream,
) linkImpl {

	lnk := &_linkImpl{
		_serverAddress: serverAddress,
		_services:      services,
		_ctrl:          ctrl,
		_streams:       createStreamManager(services),
		_datagramsChan: make(chan []byte),
		_lifecycle:     createCloserLifecycle(_linkImplOpCount),
	}

	if tcpListener != nil {
		lnk._streamAcceptor = createServerStreamAcceptor(tcpListener, lnk._streams)
	} else {
		lnk._clientStreamAcceptor = createClientStreamAcceptor()
		lnk._streamAcceptor = lnk._clientStreamAcceptor
	}

	err := lnk._lifecycle.OperationStarted(_linkImplOpControlReceiveLoop)
	if err != nil {
		panic(err)
	}
	go lnk._controlReceiveLoop()

	return lnk
}

func (lnk *_linkImpl) GetLinkInfo() *tunme.LinkInfo {
	return &tunme.LinkInfo{
		OptimalDatagramMaxSize: 1000, // TODO: sensible value + take crypto overhead in account
		DatagramMaxSize:        math.MaxInt,
	}
}

func (lnk *_linkImpl) Close() error {

	var err error

	shouldClose, err := lnk._lifecycle.CloseStarted(true)
	if !shouldClose {
		return err
	}
	defer lnk._lifecycle.CloseFinished()

	err = lnk._close()
	if err != nil {
		return err
	}

	fmt.Printf("link completly closed on our end\n")

	return nil
}

func (lnk *_linkImpl) ReceiveDatagram() ([]byte, error) {

	datagram, isStillOpen := <-lnk._datagramsChan

	if !isStillOpen {
		return nil, tunme.ClosedError
	}

	return datagram, nil
}

func (lnk *_linkImpl) SendDatagram(payload []byte) error {

	err := lnk._lifecycle.OperationStarted(-1)
	if err != nil {
		panic(err)
	}
	defer lnk._lifecycle.OperationFinished(-1)

	return lnk._ctrl.SendDatagram(payload)
}

func (lnk *_linkImpl) OpenStream() (tunme.Stream, error) {

	err := lnk._lifecycle.OperationStarted(-1)
	if err != nil {
		return nil, err
	}
	defer lnk._lifecycle.OperationFinished(-1)

	var stream tunme.Stream

	if lnk._isServer() {
		stream = lnk._streams.CreateServerStream()
		lnk._ctrl.SendOpenStream(stream.GetId())
	} else {
		stream = lnk._streams.CreateClientStream(lnk._serverAddress)
	}

	fmt.Printf("stream %d -> opened\n", stream.GetId())

	return stream, nil
}

func (lnk *_linkImpl) AcceptStream() (tunme.Stream, error) {

	err := lnk._lifecycle.OperationStarted(_linkImplOpAcceptStream)
	if err != nil {
		return nil, err
	}
	defer lnk._lifecycle.OperationFinished(_linkImplOpAcceptStream)

	stream, err := lnk._streamAcceptor.Accept()
	if err != nil {
		return nil, err
	}

	if lnk._lifecycle.IsClosed() {
		return nil, tunme.ClosedError
	}

	return stream, nil
}

func (lnk *_linkImpl) _isServer() bool {
	return lnk._serverAddress == nil
}

func (lnk *_linkImpl) _controlReceiveLoop() {

	defer lnk._lifecycle.OperationFinished(_linkImplOpControlReceiveLoop)

	for {
		msg, err := lnk._ctrl.Receive()

		if err != nil {
			if lnk._lifecycle.IsClosed() {
				break
			} else if err == io.EOF {
				fmt.Printf("link closed on the peer side\n")
				lnk._close()
				break
			} else {
				panic(err)
			}
		}

		if msg.t == controlMsgTypeDatagram {
			lnk._datagramsChan <- msg.data.(controlMsgDatagramDto).datagram
		} else if msg.t == controlMsgTypeOpenStream {
			if lnk._isServer() {
				fmt.Printf("server received a streamImpl open request (should only be received by clients)")
			} else {
				stream := lnk._streams.CreateServerStreamFromClient(msg.data.(controlMsgOpenStreamDto).streamId, lnk._serverAddress)
				fmt.Printf("stream %d <- opened\n", stream.GetId())
				lnk._clientStreamAcceptor.StreamRequestReceived(stream)
			}
		}
	}
}

func (lnk *_linkImpl) _close() error {

	// TODO: warn errors?

	lnk._closeOnce.Do(func() {

		_ = lnk._streamAcceptor.Close()

		lnk._ctrl.Close()

		close(lnk._datagramsChan)

		lnk._streams.Close()
	})

	return nil
}
