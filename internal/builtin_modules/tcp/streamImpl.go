package tcp

import (
	"fmt"
	"io"
	"net"
	"sync"
	"tunme/pkg/tunme"
)

// TODO
// max segment size including encryption overhead
const maxSegmentSize = 4096

// operation IDs used for controlling the lifecycle
const (
	_streamImplOpRead int = iota
	_streamImplOpWrite
	_streamImplOpCount
)

type streamImpl interface {
	tunme.Stream
	AttachTcpConnection(stream encryptedTcpStream)
	CloseImplicitly()
}

type _streamImpl struct {
	_id                    uint64
	_services              tunme.Services
	_closeCallback         func(stream tunme.Stream)
	_attachedData          interface{}
	_connectionAttachMutex sync.Mutex
	_connectionAttachCond  *sync.Cond
	_stream                encryptedTcpStream
	_lifecycle             closerLifecycle
}

func createStream(id uint64, services tunme.Services, closeCallback func(stream tunme.Stream)) streamImpl {

	stream := &_streamImpl{
		_id:            id,
		_services:      services,
		_closeCallback: closeCallback,
		_lifecycle:     createCloserLifecycle(_streamImplOpCount),
	}

	stream._connectionAttachCond = sync.NewCond(&stream._connectionAttachMutex)

	return stream
}

func createStreamWithConnection(id uint64, services tunme.Services, closeCallback func(stream tunme.Stream), addr *net.TCPAddr) streamImpl {

	stream := &_streamImpl{
		_id:            id,
		_services:      services,
		_closeCallback: closeCallback,
		_lifecycle:     createCloserLifecycle(_streamImplOpCount),
	}

	connection, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		panic(err)
	}

	stream._stream = createEncryptedStream(
		connection,
		stream._services.CipherFactory().CreateStreamDecryptor(connection, maxSegmentSize),
		stream._services.CipherFactory().CreateStreamEncryptor(connection, maxSegmentSize),
	)

	streamHello := streamHelloDto{
		streamId: stream.GetId(),
	}
	err = streamHello.write(stream._stream)
	if err != nil {
		panic(err)
	}
	err = stream.Flush()
	if err != nil {
		panic(err)
	}

	return stream
}

func (s *_streamImpl) GetId() uint64 {
	return s._id
}

func (s *_streamImpl) SetAttachedData(data interface{}) {
	s._attachedData = data
}

func (s *_streamImpl) GetAttachedData() interface{} {
	return s._attachedData
}

func (s *_streamImpl) Read(p []byte) (int, error) {

	err := s._lifecycle.OperationStarted(_streamImplOpRead)
	if err != nil {
		return 0, err
	}
	defer s._lifecycle.OperationFinished(_streamImplOpRead)

	if !s._waitConnectionAttached() {
		return 0, io.EOF
	}

	n, err := s._stream.Read(p)

	if n != 0 {
		fmt.Printf("stream %d <- %d bytes\n", s.GetId(), n)
	}
	if err == io.EOF {
		fmt.Printf("stream %d <- EOF\n", s.GetId())
	}

	return n, err
}

func (s *_streamImpl) Write(p []byte) (int, error) {

	err := s._lifecycle.OperationStarted(_streamImplOpWrite)
	if err != nil {
		return 0, err
	}
	defer s._lifecycle.OperationFinished(_streamImplOpWrite)

	if !s._waitConnectionAttached() {
		return 0, tunme.ClosedError
	}

	fmt.Printf("stream %d -> %d bytes\n", s.GetId(), len(p))

	return s._stream.Write(p)
}

func (s *_streamImpl) Flush() error {

	err := s._lifecycle.OperationStarted(_streamImplOpWrite)
	if err != nil {
		return err
	}
	defer s._lifecycle.OperationFinished(_streamImplOpWrite)

	s._connectionAttachMutex.Lock()
	hasConnection := s._isConnectionAttached()
	s._connectionAttachMutex.Unlock()
	if !hasConnection {
		return nil
	}

	return s._stream.Flush()
}

func (s *_streamImpl) Close() error {

	shouldClose, err := s._lifecycle.CloseStarted(true)
	if !shouldClose {
		return err
	}
	defer s._lifecycle.CloseFinished()

	return s._close()
}

func (s *_streamImpl) CloseImplicitly() {

	shouldClose, _ := s._lifecycle.CloseStarted(false)
	if !shouldClose {
		return
	}
	defer s._lifecycle.CloseFinished()

	s._close()
}

func (s *_streamImpl) AttachTcpConnection(stream encryptedTcpStream) {

	s._connectionAttachMutex.Lock()

	if s._isConnectionAttached() {
		panic("trying to attach multiple connections to a streamImpl")
	}

	s._stream = stream

	s._connectionAttachMutex.Unlock()
	s._connectionAttachCond.Broadcast()
}

// _waitConnectionAttached returns false when the stream is closed before
// being attached to a connection.
func (s *_streamImpl) _waitConnectionAttached() bool {

	s._connectionAttachMutex.Lock()
	defer s._connectionAttachMutex.Unlock()

	for !s._isConnectionAttached() {
		if s._lifecycle.IsClosed() {
			return false
		}
		s._connectionAttachCond.Wait()
	}

	return true
}

func (s *_streamImpl) _isConnectionAttached() bool {
	return s._stream != nil
}

func (s *_streamImpl) _close() error {

	var err error

	fmt.Printf("stream %d -> EOF\n", s.GetId())

	if s._connectionAttachCond != nil {
		s._connectionAttachCond.Broadcast()
	}

	s._lifecycle.CloseProceeding()

	if s._isConnectionAttached() {
		err = s._stream.Close()
	}

	s._closeCallback(s)

	return err
}
