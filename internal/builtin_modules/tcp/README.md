# Protocol

This modules first establishes a long-lived stream between the client and the server, called the *control stream*. The
handshake is performed on this stream in order to exchange key material. The control stream is then wrapped into the
stream cipher provided by the library.

Datagrams are exchanged directly through the control stream.

When the application on the client side wants to open a stream, a TCP connection is opened directly.

When the application on the server side wants to open a stream, it must first ask the client through the control stream.
The client will then open a TCP connection with the server.

TCP streams are always wrapped by a cipher, except for the first bytes of the control stream, when performing the
handshake.

## Control Stream

### Handshake Message

*These messages are sent unencrypted.*

| Field   | Size         | Description            |
|---------|--------------|------------------------|
| size    | 2 bytes      | Size of the message.   |
| message | *size* bytes | The handshake message. |

### Control Message Datagram

| Field        | Size         | Description            |
|--------------|--------------|------------------------|
| message_type | 1 byte       | Set to `0x00`.         |
| size         | 4 bytes      | Size of the datagram.  |
| datagram     | *size* bytes | The datagram.          |

### Control Message Open Stream

| Field        | Size         | Description            |
|--------------|--------------|------------------------|
| message_type | 1 byte       | Set to `0x01`.         |
| stream_id    | 8 bytes      | An unsigned integer.   |

## Regular Streams

### Stream Hello

| Field     | Size         | Description            |
|-----------|--------------|------------------------|
| stream_id | 8 bytes      | An unsigned integer.   |
