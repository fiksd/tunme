package tcp

import (
	"fmt"
	"net"
	"tunme/internal/utils"
	"tunme/pkg/tunme"
)

// This modules first establishes a TCP connection which is used to pass control
// messages and datagrams. A separate connection is opened for every tunneled streamImpl.

type Module struct {
}

func (module *Module) CreateLink(args []string, services tunme.Services) (tunme.Link, error) {

	// TODO: custom error type including complete help message

	isServer := false
	address := ""

	if len(args) != 2 {
		return nil, fmt.Errorf("module expects 2 arguments, %d given", len(args))
	}

	if args[0] == "server" {
		isServer = true
		address = args[1]
	} else if args[0] == "client" {
		isServer = false
		address = args[1]
	} else {
		return nil, fmt.Errorf("invalid mode %v, expecting \"client\" or \"server\"", args[1])
	}

	if isServer {
		return module.createServer(address, services)
	} else {
		return module.createClient(address, services)
	}
}

func (module *Module) createClient(address string, services tunme.Services) (tunme.Link, error) {

	// TODO: close TCP ctrlConnection on error

	addr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		return nil, err
	}

	ctrlConnection, err := net.DialTCP("tcp", nil, addr)
	if err != nil {
		return nil, err
	}

	var handshakeMsg []byte

	for !services.CipherFactory().IsHandshakeFinished() {

		handshakeMsg, err = services.CipherFactory().NextHandshakeStep(handshakeMsg)
		if err != nil {
			return nil, err
		}

		if handshakeMsg != nil {
			err = _sendHandshakeMessage(ctrlConnection, handshakeMsg)
			if err != nil {
				return nil, err
			}
		}

		if services.CipherFactory().IsHandshakeFinished() {
			break
		}

		handshakeMsg, err = _readHandshakeMessage(ctrlConnection)
		if err != nil {
			return nil, err
		}
	}

	ctrlStream := createControlStream(ctrlConnection, services)

	return createLinkImpl(nil, addr, services, ctrlStream), nil
}

func (module *Module) createServer(address string, services tunme.Services) (tunme.Link, error) {

	// TODO: cleanup on error

	listenAddr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		return nil, err
	}

	listener, err := net.ListenTCP("tcp", listenAddr)
	if err != nil {
		return nil, err
	}

	ctrlConnection, err := listener.AcceptTCP()
	if err != nil {
		return nil, err
	}

	for !services.CipherFactory().IsHandshakeFinished() {

		handshakeMsg, err :=  _readHandshakeMessage(ctrlConnection)
		if err != nil {
			return nil, err
		}

		handshakeMsg, err = services.CipherFactory().NextHandshakeStep(handshakeMsg)
		if err != nil {
			return nil, err
		}

		if handshakeMsg != nil {
			err = _sendHandshakeMessage(ctrlConnection, handshakeMsg)
			if err != nil {
				return nil, err
			}
		}
	}

	ctrlStream := createControlStream(ctrlConnection, services)

	return createLinkImpl(listener, nil, services, ctrlStream), nil
}

func _readHandshakeMessage(connection *net.TCPConn) ([]byte, error) {

	var sizeBuff [2]byte

	ok, err := utils.ReadBytes(connection, sizeBuff[:])
	if !ok {
		return nil, err
	}

	size := int((uint16(sizeBuff[0]) << 8) | uint16(sizeBuff[1]&0xff))

	buff := make([]byte, size)

	ok, err = utils.ReadBytes(connection, buff)
	if !ok {
		return nil, err
	}

	return buff[:size], nil
}

func _sendHandshakeMessage(connection *net.TCPConn, message []byte) error {

	buff := make([]byte, 2+len(message))

	buff[0] = byte(uint16(len(message)) >> 8)
	buff[1] = byte(uint16(len(message)) & 0xff)

	copy(buff[2:], message)

	_, err := connection.Write(buff)
	if err != nil {
		return err
	}

	return nil
}
