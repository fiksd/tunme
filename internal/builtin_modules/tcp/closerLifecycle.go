package tcp

import (
	"fmt"
	"sync"
	"tunme/pkg/tunme"
)

// TODO: use this class for streams

// This class helps ensure that:
// * Close() is called explicitly no more than once.
// * Operations such as Read() or Write() are not called concurrently with themselves.
// * Operations are correctly stopped before proceeding to the last stage of the closing process.

type closerLifecycle interface {
	OperationStarted(op int) error
	OperationFinished(op int)
	CloseStarted(explicit bool) (bool, error)
	// CloseProceeding notifies the stage of the closing process when all
	// running operations should have been terminated. This call will block until
	// this is effectively the case.
	CloseProceeding()
	CloseFinished()
	IsClosed() bool
}

type _closerLifecycle struct {
	_mutex              sync.Mutex
	_isClosed           bool
	_isClosedExplicitly bool
	_operations         []bool
	_waitGroup          sync.WaitGroup
}

func createCloserLifecycle(opCount int) closerLifecycle {
	return &_closerLifecycle{
		_operations: make([]bool, opCount),
	}
}

// OperationStarted notifies that an operation was started. It will return an error
// if the same operation is currently running. You can use an operation code of -1
// to allow concurrent executions of an operation.
func (l *_closerLifecycle) OperationStarted(op int) error {

	l._mutex.Lock()
	defer l._mutex.Unlock()

	if l._isClosed {
		return tunme.ClosedError
	}

	l._waitGroup.Add(1)

	if op >= 0 {

		if l._operations[op] {
			return fmt.Errorf("operation is already running")
		}

		l._operations[op] = true
	}

	return nil
}

func (l *_closerLifecycle) OperationFinished(op int) {

	l._mutex.Lock()
	defer l._mutex.Unlock()

	if op >= 0 {

		if !l._operations[op] {
			panic("operation was not started")
		}

		l._operations[op] = false
	}

	l._waitGroup.Done()
}

func (l *_closerLifecycle) CloseStarted(explicit bool) (bool, error) {

	l._mutex.Lock()
	defer l._mutex.Unlock()

	if l._isClosed {
		if explicit && !l._isClosedExplicitly {
			l._isClosedExplicitly = true
			return false, nil
		} else {
			return false, tunme.ClosedError
		}
	}

	l._isClosed = true
	l._isClosedExplicitly = explicit

	return true, nil
}

func (l *_closerLifecycle) CloseProceeding() {
	l._waitGroup.Wait()
}

func (l *_closerLifecycle) CloseFinished() {
	l._waitGroup.Wait()
}

func (l *_closerLifecycle) IsClosed() bool {

	// TODO: avoid using a mutex for this?

	l._mutex.Lock()
	defer l._mutex.Unlock()

	return l._isClosed
}
